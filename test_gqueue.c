#include <glib.h>
#include <stdio.h>
#include <gmodule.h>

/*Este programa guarda los elementos de argv en una GQueue*/

void print_GQueue(GQueue* q){

  guint n = q -> length;
  printf("[");

  for (unsigned int i = 0; i < (unsigned int)n ; i++) {
    printf(((i+1) < n) ?"%s," : "%s",
    (char*)g_queue_peek_nth(q,(guint)i));
  }

  printf("]\n");
}

int main(int argc, char const *argv[]) {

  GQueue *queue = g_queue_new();

  for (unsigned int i = 0; i < argc ; i++) {
    g_queue_push_tail(queue, (void*)argv[i]);
  }
  print_GQueue(queue);
  return 0;
}
